output "fw_name" {
  value = "${google_compute_firewall.firewall.name}"
}
output "fw_network" {
  value = "${google_compute_firewall.firewall.network}"
}
output "fw_source_ranges" {
  value = "${google_compute_firewall.firewall.source_ranges}"
}
output "fw_source_tags" {
  value = "${google_compute_firewall.firewall.source_tags}"
}
