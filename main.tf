# Google Cloud firewall
resource "google_compute_firewall" "firewall" {
  name    = "${var.service}-${var.fw_proto}"
  network = "${var.net_name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "${var.fw_proto}"
    ports    = ["${split(",", var.fw_ports)}"]
  }

  source_ranges = ["${split(",", var.allowed_ips)}"]

  target_tags = ["${split(",", var.target_tags)}"]
}
