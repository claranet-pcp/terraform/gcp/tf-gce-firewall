# Firewall variables
variable "service" {
  description = "The name of the service this firewall rule is for, will be interpolated at position 1 in the firewall name"
  type = "string"
}

variable "target_tags" {
  description = "Comma seperated list of target tags to set"
  type = "string"
}

variable "net_name" {
  description = "The name of the network this firewall should serve"
  type = "string"
}

variable "fw_proto" {
  description = "The protocol this firewall will allow"
  type = "string"
}

variable "fw_ports" {
  description = "Comma seperated list of ports to allow"
  type = "string"
}

variable "allowed_ips" {
  description = "Comma seperated list of IPs to allow traffic from"
  type = "string"
}
